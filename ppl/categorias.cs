﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ppl
{
    public partial class categorias : Form
    {
        data datos = new data();

        public categorias()
        {
            InitializeComponent();
        }

        private void categorias_Load(object sender, EventArgs e)
        {
            cargar();
            traducir();
        }

        public void traducir()
        {
            string lan = ppl.Properties.Settings.Default.language.ToLower();
            if (lan == "en")
            {
                translate tran = new translate();
                //label2.Text = tran.getWord(label2.Text.ToLower());
                foreach (Control item in this.Controls)
                {
                    string translated = tran.getWord(item.Text.ToLower());
                    item.Text = tran.UppercaseFirst(translated);
                    if (item is GroupBox)
                    {
                        foreach (Control GroupBoxItem in item.Controls)
                        {
                            if (!(GroupBoxItem is PictureBox))
                            {
                                string GroupBoxTranslatedItem = tran.getWord(GroupBoxItem.Text);
                                GroupBoxItem.Text = tran.UppercaseFirst(GroupBoxTranslatedItem);
                            }

                        }
                    }

                }
            }
        }

        public void cargar()
        {
            dataGridView1.DataSource = datos.getTable("categories",string.Empty);
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            txtname.Text = dataGridView1.CurrentRow.Cells[1].Value.ToString();
            if (dataGridView1.CurrentRow.Cells[4].Value.Equals(true))
            {
                txtactive.Select();
            }
            else
            {
                txtactiveno.Select();
            }
            txtdescription.Text = dataGridView1.CurrentRow.Cells[2].Value.ToString();
            label4.Text = "Total de productos en la categoria "+txtname.Text+": "+datos.getCount("products","*","categoria='"+txtname.Text+"'");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (datos.delete("categories", "id=" + dataGridView1.CurrentRow.Cells[0].Value + ""))
            {
                cargar();
                MessageBox.Show("Categoria eliminada");
            }
            else
            {
                MessageBox.Show("No se ha podido completar este proceso");
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (datos.update("categories", "activa=0", "id=" + dataGridView1.CurrentRow.Cells[0].Value + ""))
            {
                cargar();
                MessageBox.Show("Categoria desactivada");
            }
            else
            {
                MessageBox.Show("No se ha podido completar este proceso");
            }   
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (datos.update("categories", "activa=1", "id=" + dataGridView1.CurrentRow.Cells[0].Value + ""))
            {
                cargar();
                MessageBox.Show("Categoria activada");
            }
            else
            {
                MessageBox.Show("No se ha podido completar este proceso");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string nombre = "'" + txtname.Text + "',";
            string desc = "'" + txtdescription.Text + "',";
            int active;
            DateTime fech = DateTime.Now;
            
            if (txtactive.Checked)
            {
                active = 1;
            }
            else
            {
                active = 0;
            }
            if (datos.insert("categories", "nombre,descripcion,activa,fechacreacion", nombre + desc + active + ",'" + fech + "'"))
            {
                MessageBox.Show("Categoria registrada");
                cargar();
                txtactiveno.Select();
                txtdescription.Clear();
                txtname.Clear();
            }
            else
            {
                MessageBox.Show("Ups algo no salio bien");
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            cargar();
        }
    }
}
