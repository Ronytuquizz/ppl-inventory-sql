﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("PPL Inventory")]
[assembly: AssemblyDescription("This is a quick made program to manage your items and products you are selling. any question or support feel free to contact me at (RONEL.CRUZ.A8@GMAIL.COM) or (RONYCRUZ.COM)")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Ronel Cruz (RONEL.CRUZ.A8@GMAIL.COM)")]
[assembly: AssemblyProduct("PPL Inventory")]
[assembly: AssemblyCopyright("Copyright © All rights reserved  2020")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible
// to COM components.  If you need to access a type in this assembly from
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("56f06ec9-6f7b-4e29-90fe-5b07847f5256")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
