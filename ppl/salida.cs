﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ppl
{
    public partial class salida : Form
    {
        data datos = new data();
        Double total = 0.0;

        public salida()
        {
            InitializeComponent();
        }

        private void salida_Load(object sender, EventArgs e)
        {
            cargarIn();
            traducir();
        }

        public void traducir()
        {
            string lan = ppl.Properties.Settings.Default.language.ToLower();
            if (lan == "en")
            {
                translate tran = new translate();
                //label2.Text = tran.getWord(label2.Text.ToLower());
                foreach (Control item in this.Controls)
                {
                    string translated = tran.getWord(item.Text.ToLower());
                    item.Text = tran.UppercaseFirst(translated);
                    if (item is GroupBox)
                    {
                        foreach (Control GroupBoxItem in item.Controls)
                        {
                            if (!(GroupBoxItem is PictureBox) && !(GroupBoxItem is ComboBox) && !(GroupBoxItem is TextBox))
                            {
                                string GroupBoxTranslatedItem = tran.getWord(GroupBoxItem.Text);
                                GroupBoxItem.Text = tran.UppercaseFirst(GroupBoxTranslatedItem);
                            }

                        }
                    }

                }
            }
        }

        public void cargarIn()
        {
           dataGridView1.DataSource = datos.getTable("products",string.Empty);
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string word = txtsearch.Text;
            if (word.Length>1)
            {
                dataGridView1.DataSource = datos.getTable("products", "codigo='" + word + "'");
            }
            else
            {
                dataGridView1.DataSource = datos.getTable("products", string.Empty);
            }
        }

        private void linkLabel3_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            
        }

        public void calcularTotal(string opt)
        {
            if (dataGridView2.Rows.Count<1)
            {
                total = 0.0;
                txtttl.Text = total.ToString();
                return;
            }

            switch (opt)
            {
                case "add": {
                        total = 0.0;
                        foreach (DataGridViewRow item in dataGridView2.Rows)
                        {
                            total = total + Convert.ToInt32(item.Cells[4].Value);
                            //MessageBox.Show(item.Cells[4].Value.ToString());
                        }
                    } break;
                case "sub": {
                        total = total - Convert.ToInt32(dataGridView2.CurrentRow.Cells[4].Value);
                    } break;
                default:
                    break;
            }
            
                    
            
            txtttl.Text = total.ToString();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (DataGridViewRow fila in dataGridView2.Rows)
                {
                    if (fila.Cells[0].Value.ToString() == dataGridView1.CurrentRow.Cells[0].Value.ToString())
                    {
                        MessageBox.Show("Este producto ya existe en la factura");
                        return;
                    }
                }
                if (txtcantidad.Text == "" || txtcantidad.Text == "0")
                {
                    MessageBox.Show("Debes introducir una cantidad valida");
                    return;
                }
                if (txtdescuento.Text == "")
                {
                    MessageBox.Show("De no usar descuento debes dejarlo en 0");
                    return;
                }
                int qty = Convert.ToInt32(txtcantidad.Text);
                int mayorprice = Convert.ToInt32(dataGridView1.CurrentRow.Cells[6].Value);
                int price = Convert.ToInt32(dataGridView1.CurrentRow.Cells[5].Value);
                int discount = Convert.ToInt32(txtdescuento.Text);
                int calculo;
                int existencia = Convert.ToInt32(dataGridView1.CurrentRow.Cells[7].Value);
                
                if (qty > existencia)
                {
                    MessageBox.Show("No tienes productos suficientes para realizar esta factura");
                    return;
                }
                if (txtpormayor.Checked)
                {
                    calculo = (mayorprice * qty);
                }
                else
                {
                    calculo = (price * qty);
                }
                dataGridView1.CurrentRow.Cells[7].Value = Convert.ToInt32(dataGridView1.CurrentRow.Cells[7].Value) - qty;
                dataGridView2.Rows.Add(dataGridView1.CurrentRow.Cells[0].Value, dataGridView1.CurrentRow.Cells[1].Value, txtcantidad.Text, txtdescuento.Text, calculo);
                calcularTotal("add");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ups algo paso haciendo los calculos, asegurate que el producto tenga la info en formato correcto, a continuacion veras mas detalles del problema");
                MessageBox.Show(ex.ToString());
                
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (dataGridView2.Rows.Count>=1)
            {
                foreach (DataGridViewRow item in dataGridView1.Rows)
                {
                    string id = dataGridView2.CurrentRow.Cells[0].Value.ToString();
                    if (item.Cells[0].Value.ToString().Equals(id))
                    {
                        item.Cells[7].Value = Convert.ToInt32(item.Cells[7].Value) + Convert.ToInt32(dataGridView2.CurrentRow.Cells[2].Value);
                        //total = total - Convert.ToInt32(dataGridView2.CurrentRow.Cells[4].Value);                        
                    }
                }
                DataGridViewRow fila = dataGridView2.CurrentRow;
                calcularTotal("sub");
                dataGridView2.Rows.Remove(fila);
                
            }
            else
            {
                MessageBox.Show("No hay elementos a remover en la factura.");
            }
        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {

        }

        private void linkLabel4_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {

        }

        private void button7_Click(object sender, EventArgs e)
        {
            cargarIn();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            inventory hp = new inventory();
            hp.Show();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (DataGridViewRow fila in dataGridView2.Rows)
                {
                    string id = fila.Cells[0].Value.ToString();
                    string cantidad = fila.Cells[2].Value.ToString();
                    string newqt = Convert.ToString(Convert.ToInt32(datos.getSpecificValue("products", "existencia", "id=" + id)) - Convert.ToInt32(cantidad));
                    if (!datos.update("products", "existencia=" + newqt, "id=" + id))
                    {
                        throw new Exception("No se pudo completar laa modificacion");
                    }
                }
                dataGridView2.Rows.Clear();
                cargarIn();
                MessageBox.Show("Cambios aplicados correctamente");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                throw;
            }   
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            label4.Text = "Codigo: "+dataGridView1.CurrentRow.Cells[9].Value.ToString();
            label6.Text = "Precio: "+dataGridView1.CurrentRow.Cells[5].Value.ToString()+" / "+ dataGridView1.CurrentRow.Cells[6].Value.ToString();
            label4.ForeColor = Color.Red;
            label6.ForeColor = Color.Red;
        }
    }
}
