﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ppl
{
    public partial class home : Form
    {
        string usuario;
        public home(string usr)
        {
            InitializeComponent();
            usuario = usr;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            inventory rf = new inventory();
            rf.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            AboutBox ffff = new AboutBox();
            ffff.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            entrada fo = new entrada();
            fo.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            categorias fp = new categorias();
            fp.Show();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            usuarios ko = new usuarios();
            ko.Show();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            login kk = new login();
            kk.Show();
            this.Hide();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            salida g = new salida();
            g.Show();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            setting gg = new setting();
            gg.Show();
        }

        private void home_Load(object sender, EventArgs e)
        {
            this.Text = "Home - Iniciado como [" + usuario+"]";
            traducir();
        }
        public void traducir()
        {
            string lan = ppl.Properties.Settings.Default.language.ToLower();
            if (lan == "en")
            {
                translate tran = new translate();
                //label2.Text = tran.getWord(label2.Text.ToLower());
                foreach (ToolStripItem item in menuStrip1.Items)
                {
                    string translated = tran.getWord(item.Text.ToLower());
                    item.Text = tran.UppercaseFirst(translated);                    
                }
                foreach (Control item in this.Controls)
                {
                    string translated = tran.getWord(item.Text.ToLower());
                    item.Text = tran.UppercaseFirst(translated);
                    if (item is GroupBox)
                    {
                        foreach (Control GroupBoxItem in item.Controls)
                        {
                            if (!(GroupBoxItem is PictureBox))
                            {
                                string GroupBoxTranslatedItem = tran.getWord(GroupBoxItem.Text);
                                GroupBoxItem.Text = tran.UppercaseFirst(GroupBoxTranslatedItem);
                            }

                        }
                    }

                }
            }
        }

        private void productoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void entradaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            entrada ll = new entrada();
            ll.Show();
        }

        private void salidaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            salida pp = new salida();
            pp.Show();
        }

        private void categoriasDeProductoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            categorias kk = new categorias();
            kk.Show();
        }

        private void proveedoresToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void usuarioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            usuarios ii = new usuarios();
            ii.Show();
        }

        private void inventarioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            inventory rf = new inventory();
            rf.Show();
        }

        private void ventasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //entrada
        }

        private void gestionDeUsuariosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            usuarios ko = new usuarios();
            ko.Show();
        }

        private void gestionDeEmpresaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            setting gg = new setting();
            gg.Show();
        }

        private void confguracionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            setting gg = new setting();
            gg.Show();
        }

        private void informacionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutBox ffff = new AboutBox();
            ffff.Show();
        }
    }
}
