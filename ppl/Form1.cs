﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ppl
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //login f = new login();
            //f.Show();
            label1.Text = ppl.Properties.Settings.Default.companyName;
            traducir();
            timer1.Start();
        }

        public void traducir()
        {
            string lan = ppl.Properties.Settings.Default.language.ToLower();
            if (lan == "en")
            {
                translate tran = new translate();
                //label2.Text = tran.getWord(label2.Text.ToLower());
                foreach (Control item in this.Controls)
                {
                    string translated = tran.getWord(item.Text.ToLower());
                    item.Text = tran.UppercaseFirst(translated);
                    if (item is GroupBox)
                    {
                        foreach (Control GroupBoxItem in item.Controls)
                        {
                            if (!(GroupBoxItem is PictureBox))
                            {
                                string GroupBoxTranslatedItem = tran.getWord(GroupBoxItem.Text);
                                GroupBoxItem.Text = tran.UppercaseFirst(GroupBoxTranslatedItem);
                            }

                        }
                    }

                }
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (progressBar1.Value<100)
            {
                progressBar1.Value++;
            }
            else
            {
                timer1.Stop();
                progressBar1.Value = 100;
                this.Hide();
                login f = new login();
                f.Show();
            }
            
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
