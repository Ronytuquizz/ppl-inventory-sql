﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ppl
{
    public partial class inventory : Form
    {
        data datos = new data();

        bool isEditing = false;

        public inventory()
        {
            InitializeComponent();
        }

        private void inventory_Load(object sender, EventArgs e)
        {
            loadCategories();
            cargar();
            if (!isEditing)
            {
                groupBox1.Enabled = false;
            }
            traducir();
        }

        public void traducir()
        {
            string lan = ppl.Properties.Settings.Default.language.ToLower();
            if (lan == "en")
            {
                translate tran = new translate();
                //label2.Text = tran.getWord(label2.Text.ToLower());
                foreach (Control item in this.Controls)
                {
                    string translated = tran.getWord(item.Text.ToLower());
                    item.Text = tran.UppercaseFirst(translated);
                    if (item is GroupBox)
                    {
                        foreach (Control GroupBoxItem in item.Controls)
                        {
                            if (!(GroupBoxItem is PictureBox) && !(GroupBoxItem is ComboBox) && !(GroupBoxItem is TextBox))
                            {
                                string GroupBoxTranslatedItem = tran.getWord(GroupBoxItem.Text);
                                GroupBoxItem.Text = tran.UppercaseFirst(GroupBoxTranslatedItem);
                            }

                        }
                    }

                }
            }
        }

        public void cargar()
        {
            data go = new data();
            dataGridView1.AutoGenerateColumns = true;
            //DataTable dt = go.getTable("products", string.Empty).Tables[0];
            dataGridView1.DataSource = go.getTable("products", string.Empty);
        }

        public void loadCategories()
        {
            var list = datos.getTable("categories", "activa=1");
            txtcategoria.ValueMember = "id";
            txtcategoria.DisplayMember = "nombre";
            txtcategoria.DataSource = list;

            comboBox1.ValueMember = "id";
            comboBox1.DisplayMember = "nombre";
            comboBox1.DataSource = list;

        }
        private void button1_Click(object sender, EventArgs e)
        {
            
            if (!isEditing)
            {
                loadCategories();
                isEditing = true;
                groupBox1.Enabled = true;
                dataGridView1.Enabled = false;
                button1.Text = "Guardar";
            }
            else
            {
                string name = "'"+txtnombre.Text+"',";
                string code = "'" + txtcodigo.Text + "',";
                string category = "'" + txtcategoria.Text + "',";
                string date = "'" + txtfecha.Text + "',";
                string pricebuy = "'" + txtpreciocompra.Text + "',";
                string pricesale = "'" + txtprecioventa.Text + "',";
                string pricewhole= "'" + txtpreciomayor.Text + "',";
                string qty = "'" + txtexistencia.Text + "',";
                string description = "'" + txtdescripcion.Text + "'";


                if (datos.update("products","nombre="+name+"codigo="+code+"categoria="+category+"fechacompra="+date+"preciocompra="+pricebuy+"precioventa="+pricesale+"preciomayor="+pricewhole+"existencia="+qty+"descripcion="+description, "id=" + dataGridView1.CurrentRow.Cells[0].Value.ToString()))
                {
                    isEditing = false;
                    groupBox1.Enabled = false;
                    button1.Text = "Editar este producto";
                    dataGridView1.Enabled = true;
                    cargar();
                }
            }
        }

        private void dataSet1BindingSource_CurrentChanged(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            cargar();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (datos.delete("products", "id=" + dataGridView1.CurrentRow.Cells[0].Value + ""))
            {
                cargar();
                MessageBox.Show("Producto eliminada");
            }
            else
            {
                MessageBox.Show("No se ha podido completar este proceso");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string word = txtsearch.Text;
            if (word.Length > 1)
            {
                dataGridView1.DataSource = datos.getTable("products", "codigo like '%" + word + "%'");
            }
            else
            {
                dataGridView1.DataSource = datos.getTable("products", string.Empty);
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            pasar();
        }

        public void pasar()
        {
            try
            {
                button1.Enabled = true;
                string id = dataGridView1.CurrentRow.Cells[0].Value.ToString();
                string codigo = dataGridView1.CurrentRow.Cells[1].Value.ToString();
                string nombre = dataGridView1.CurrentRow.Cells[2].Value.ToString();
                string categoria = dataGridView1.CurrentRow.Cells[3].Value.ToString();
                string fechacompra = dataGridView1.CurrentRow.Cells[4].Value.ToString();
                string preciocompra = dataGridView1.CurrentRow.Cells[5].Value.ToString();
                string precioventa = dataGridView1.CurrentRow.Cells[6].Value.ToString();
                string preciomayor = dataGridView1.CurrentRow.Cells[7].Value.ToString();
                string existencia = dataGridView1.CurrentRow.Cells[8].Value.ToString();
                string descripcion= dataGridView1.CurrentRow.Cells[9].Value.ToString();
                
                txtcodigo.Text = codigo;
                txtnombre.Text = nombre;
                txtcategoria.Text = categoria;
                txtfecha.Text = fechacompra;
                txtpreciocompra.Text = preciocompra;
                txtprecioventa.Text = precioventa;
                txtpreciomayor.Text = preciomayor;
                txtexistencia.Text = existencia;
                txtdescripcion.Text = descripcion;
                //MessageBox.Show(id);
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            loadCategories();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            categorias ll = new categorias();
            ll.Show();
        }

        private void linkLabel3_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            loadCategories();
        }

        private void groupBox3_Enter(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {
            string cat = comboBox1.Text;
            if (cat.Length > 1)
            {
                dataGridView1.DataSource = datos.getTable("products", "categoria= '"+ cat + "'");
            }
            else
            {
                dataGridView1.DataSource = datos.getTable("products", string.Empty);
            }
        }
    }
}
