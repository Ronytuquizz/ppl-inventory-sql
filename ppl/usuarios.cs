﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ppl
{
    public partial class usuarios : Form
    {
        data datos = new data();

        public usuarios()
        {
            InitializeComponent();
        }

        private void usuarios_Load(object sender, EventArgs e)
        {
            cargar();
            traducir();
        }

        public void traducir()
        {
            string lan = ppl.Properties.Settings.Default.language.ToLower();
            if (lan == "en")
            {
                translate tran = new translate();
                //label2.Text = tran.getWord(label2.Text.ToLower());
                foreach (Control item in this.Controls)
                {
                    string translated = tran.getWord(item.Text.ToLower());
                    item.Text = tran.UppercaseFirst(translated);
                    if (item is GroupBox)
                    {
                        foreach (Control GroupBoxItem in item.Controls)
                        {
                            if (!(GroupBoxItem is PictureBox) && !(GroupBoxItem is ComboBox) && !(GroupBoxItem is TextBox))
                            {
                                string GroupBoxTranslatedItem = tran.getWord(GroupBoxItem.Text);
                                GroupBoxItem.Text = tran.UppercaseFirst(GroupBoxTranslatedItem);
                            }

                        }
                    }

                }
            }
        }

        public void cargar()
        {
            dataGridView1.DataSource = datos.getTable("users", string.Empty);
            //foreach (DataGridViewRow item in dataGridView1.Rows)
            //{
            //    if (item.Cells[3].Value.ToString() == "1")
            //    {
            //        item.Cells[3].ValueType = typeof(string);
            //        item.Cells[3].Value = "si";
            //    }
            //    else
            //    {
            //        item.Cells[3].Value = "no";
            //    }
            //}
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (datos.delete("users", "id=" + dataGridView1.CurrentRow.Cells[0].Value + ""))
            {
                cargar();
                MessageBox.Show("Usuario eliminado");
            }
            else
            {
                MessageBox.Show("No se ha podido completar este proceso");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string nombre = "'" + txtname.Text + "',";
            string desc = "'" + txtdescription.Text + "',";
            int active;
            DateTime fech = DateTime.Now;

            if (txtactive.Checked)
            {
                active = 1;
            }
            else
            {
                active = 0;
            }
            if (datos.insert("users", "usuario,clave,activo", nombre + desc + active))
            {
                MessageBox.Show("Usuario registrado");
                cargar();
                txtactiveno.Select();
                txtdescription.Clear();
                txtname.Clear();
            }
            else
            {
                MessageBox.Show("Ups algo no salio bien");
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (datos.update("users", "activo=0", "id=" + dataGridView1.CurrentRow.Cells[0].Value + ""))
            {
                cargar();
                MessageBox.Show("Usuario desactivada");
            }
            else
            {
                MessageBox.Show("No se ha podido completar este proceso");
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (datos.update("users", "activo=1", "id=" + dataGridView1.CurrentRow.Cells[0].Value + ""))
            {
                cargar();
                MessageBox.Show("Usuario activado");
            }
            else
            {
                MessageBox.Show("No se ha podido completar este proceso");
            }
        }
    }
}
