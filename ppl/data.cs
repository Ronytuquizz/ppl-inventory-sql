﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ppl
{
    class data
    {
        static string ruta = ppl.Properties.Settings.Default.DBpath;
        static string connectionString = @"Data Source=(localdb)\ProjectsV12;Initial Catalog=ppl;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
        SqlConnection connect = new SqlConnection(connectionString);
        public bool insert(string table,string fields,string values)
        {
            try
            {
                SqlCommand command = new SqlCommand("insert into "+table+" ("+fields+")values("+values+");", connect);
                
                connect.Open();
                command.ExecuteNonQuery();
                connect.Close();
                return true;
            }
            catch (Exception pp)
            {
                return false;
            }
            finally
            {
                connect.Close();
            }
            
        }
        public string getSpecificValue(string table, string field, string conditions)
        {
            try
            {
                if (conditions == string.Empty)
                {
                    conditions = "1=1";
                }
                SqlCommand command = new SqlCommand("select "+field+" from " + table + " where " + conditions + ";", connect);
                SqlDataAdapter da = new SqlDataAdapter(command);

                DataTable dt = new DataTable();
                da.Fill(dt);

                if (dt.Rows.Count >= 1)
                {
                    DataRow row = dt.Rows[0];

                    string salida = row[field].ToString();
                    return salida;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ups, no hemos podido acceder a la base de datos (" + ex.Message + ")");
                return null;
            }
        }
        public string getSingleData(string table, string conditions)
        {
            try
            {
                if (conditions == string.Empty)
                {
                    conditions = "1=1";
                }
                SqlCommand command = new SqlCommand("select * from " + table + " where " + conditions + ";", connect);
                SqlDataAdapter da = new SqlDataAdapter(command);

                DataTable dt = new DataTable();
                da.Fill(dt);

                if (dt.Rows.Count >= 1)
                {
                    DataRow row = dt.Rows[0];

                    string salida = row["en"].ToString();
                    return salida;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ups, no hemos podido acceder a la base de datos ("+ex.Message+")");
                return null;
            }
        }

        public string getCount(string table, string column_name,string conditions)
        {
            try
            {
                if (conditions == string.Empty)
                {
                    conditions = "1=1";
                }
                SqlCommand command = new SqlCommand("select count(" + column_name + ") as totalReturned from " + table + " where " + conditions +";", connect);
                SqlDataAdapter da = new SqlDataAdapter(command);
                DataTable dt = new DataTable();
                da.Fill(dt);
                if (dt.Rows.Count >= 1)
                {
                    DataRow row = dt.Rows[0];
                    string salida = row["totalReturned"].ToString();
                    return salida;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ups, no hemos podido acceder a la base de datos (" + ex.Message + ")");
                
                return null;
            }
        }
        public DataTable getTable(string table,string conditions)
        {
            try
            {
                if (conditions == string.Empty)
                {
                    conditions = "1=1";
                }
                SqlCommand command = new SqlCommand("select * from " + table + " where " + conditions + ";", connect);
                SqlDataAdapter da = new SqlDataAdapter(command);

                DataTable dt = new DataTable();
                da.Fill(dt);

                if (dt.Rows.Count >= 1)
                {
                    return dt;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return null;
            }
        }
        public bool login(string user, string pass)
        {
            SqlCommand command = new SqlCommand("select * from users where usuario='"+user+"' and clave='"+pass+"' and activo = 1", connect);
            SqlDataAdapter da = new SqlDataAdapter(command);
            DataSet dset = new DataSet();
            da.Fill(dset);
            if (dset.Tables[0].Rows.Count == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool update(string table,string modifications,string conditions)
        {
            try
            {
                if (conditions == string.Empty)
                {
                    conditions = "1=1";
                }
                SqlCommand command = new SqlCommand("UPDATE "+table+" SET "+modifications+" WHERE "+conditions+";", connect);
                connect.Open();
                command.ExecuteNonQuery();
                connect.Close();
                return true;
            }
            catch (Exception)
            {
                return false;
                throw;
            }
            finally
            {
                connect.Close();
            }
        }
        public bool delete(string table, string conditions)
        {
            try
            {
                SqlCommand command = new SqlCommand("DELETE FROM "+table+" WHERE "+conditions+";", connect);
                connect.Open();
                command.ExecuteNonQuery();
                connect.Close();
                return true;
            }
            catch (Exception)
            {
                return false;
                throw;
            }
            finally
            {
                connect.Close();
            }
        }
        public bool userExit(string username)
        {            
            //set up connection string
            SqlCommand command = new SqlCommand("select * from users where usuario='"+username+"'", connect);
            SqlParameter param0 = new SqlParameter("@login", SqlDbType.VarChar);

            param0.Value = "employeeID.Text";
            command.Parameters.Add(param0);

            //middle tier to run connect
            SqlDataAdapter da = new SqlDataAdapter(command);

            DataSet dset = new DataSet();

            da.Fill(dset);

            if (dset.Tables[0].Rows.Count == 1)
            {
                return true;
            }
            else {
                return false;
            }

            //if (dset.Tables[0].Rows[0]["clave"].ToString().Equals("password.Text"))
            //{
            //    return true;
            //}

            
        }
    }
}
